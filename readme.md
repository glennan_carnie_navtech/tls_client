# TLS CLIENT

Basic TLS client.

## Build
This project is supplied with a basic CMake file.  The CMakeLists.txt is found in the `build` directory.
To build:
```
$ cd build
$ cmake .
$ make
```

The output executable (`tls_client`) is located in the `build\bin` sub-folder.

## Notes
* The TLS client currently only has placeholder code for the TLS element.  It DOES NOT perform any TLS functionality.