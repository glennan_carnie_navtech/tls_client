cmake_minimum_required(VERSION 3.0.0)

set(EXE_NAME      tls_client)
set(OUTPUT_DIR    ./bin)

set(SRC_ROOT      ../source)

set(APP_SRC       ${SRC_ROOT}/tls_client)
set(UTILITY_SRC   ${SRC_ROOT}/utilities)
set(SECURITY_SRC  ${SRC_ROOT}/security)

set(APP_INC       ${SRC_ROOT}/tls_client)
set(UTILITY_INC   ${SRC_ROOT}/utilities)
set(SECURITY_INC  ${SRC_ROOT}/security)
set(TEST_INC      ${SRC_ROOT}/test)
set(ASIO_INC      ${SRC_ROOT}/asio-1.18.1/include)
set(BOTAN_INC     /usr/include/botan-2)

message(STATUS "Source directory - ${SRC_ROOT}")

project(${EXE_NAME})

add_compile_options(-Wall -Wextra -g3 -O0 -std=c++17 -ggdb3)
add_definitions(-DTRACE_ENABLED -DRECEIVER)
set (CMAKE_EXE_LINKER_FLAGS "-Xlinker -Map=output.map")

include_directories(${APP_INC} ${ASIO_INC} ${TEST_INC} ${BOTAN_INC} ${UTILITY_INC} ${SECURITY_INC})

link_libraries(-pthread botan-2)

aux_source_directory(${APP_SRC} APP_SOURCE_FILES)
aux_source_directory(${UTILITY_SRC} UTILITY_SOURCE_FILES)
aux_source_directory(${SECURITY_SRC} SECURITY_SOURCE_FILES)

set(CMAKE_RUNTIME_OUTPUT_DIRECTORY ${OUTPUT_DIR})

add_executable(${EXE_NAME} ${APP_SOURCE_FILES} ${UTILITY_SOURCE_FILES} ${SECURITY_SOURCE_FILES})