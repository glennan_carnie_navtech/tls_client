#include <fstream>

#include "botan/x509self.h"
#include "botan/system_rng.h"
#include "botan/ecdsa.h"

#include "Certificate_file.h"
#include "Key_file.h"

namespace Security {

    Certificate_file::Certificate_file(const std::string& certificate_filename) :
        file_name { certificate_filename }
    {
    }


    bool Certificate_file::exists() const
    {
        std::fstream certificate_file { };
        certificate_file.open(file_name);
        return !certificate_file.fail();
    }


    Certificate_file::operator bool() const
    {
        return exists();
    }


    const std::string& Certificate_file::filename() const
    {
        return file_name;
    }


    void Certificate_file::create(const Key_file& key_file)
    {
        Botan::X509_Cert_Options options("", 6000 * 24 * 60 * 60);

        options.common_name     = "epu.navtechradar.co.uk";
        options.country         = "GB";
        options.organization    = "Navtech Radar Ltd";
        options.email           = "navtech.admin@navtechradar.com";
        options.more_dns.push_back("127.0.0.1");

        auto certificate = Botan::X509::create_self_signed_cert(options, key_file.key(), "SHA-256", Botan::system_rng());
        
        std::fstream certificate_file { };
        certificate_file.open(file_name, std::ios::out | std::ios::trunc);
        certificate_file << certificate.PEM_encode();
        certificate_file.close();
    }


} // namespace Security