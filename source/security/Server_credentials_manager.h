#pragma once

#include "Credentials_manager.h"

#include <vector>
#include <string>

#include "botan/pubkey.h"
#include "botan/rsa.h"


namespace Security {

	class Server_credentials_manager : public Credentials_manager {
	public:
		void set_certificate(const std::string& server_cert);
		void set_key(const std::string& server_key, const std::string& passphrase);
		bool load();
	
	protected:
		std::vector<Botan::X509_Certificate> cert_chain(
			const std::vector<std::string>& algos, 
			const std::string& type, 
			const std::string& hostname) override;

		Botan::Private_Key* private_key_for(
			const Botan::X509_Certificate& certificate, 
			const std::string& type, 
			const std::string& context) override;

	private:
		std::string certificate;
		std::string key;
		std::string passphrase;

		struct Certificate_info {
			std::vector<Botan::X509_Certificate> certificates;
			std::shared_ptr<Botan::Private_Key>  key;
		};

		std::vector<Certificate_info> credentials;
	};


} // namespace Security
