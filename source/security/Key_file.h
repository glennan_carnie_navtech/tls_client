#ifndef KEY_FILE_H
#define KEY_FILE_H

#include <string>
#include <memory>
#include <botan/ecc_key.h>
#include <botan/ecdsa.h>


namespace Security {

    class Key_file {
    public:
        Key_file(const std::string& key_filename);
        bool exists() const;
        operator bool() const;
        void create();
        const Botan::ECDSA_PrivateKey& key() const;
        const std::string& filename() const;

    private:
        std::string file_name { };
        
        using Key_pointer = std::unique_ptr<Botan::ECDSA_PrivateKey>;
        Key_pointer private_key { nullptr };
    };

} // namespace Security


#endif  // KEY_FILE_H