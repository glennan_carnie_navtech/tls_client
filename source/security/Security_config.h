#ifndef SECURITY_CONFIG_H
#define SECURITY_CONFIG_H

namespace Security {

    struct Config {
        std::string   certificate;
        std::string   key;
        std::string   passphrase;
    };

} // namespace Security

#endif // SECURITY_CONFIG_H