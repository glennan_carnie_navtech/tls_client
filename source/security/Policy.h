#ifndef POLICY_H
#define POLICY_H

#include "botan/tls_policy.h"

namespace Security {

    class TLS_policy : public Botan::TLS::Strict_Policy {
	public:
		bool require_cert_revocation_info() const override;
	};

} // namespace Security


#endif // POLICY_H