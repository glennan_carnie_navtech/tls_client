#ifndef CERTIFICATE_FILE_H
#define CERTIFICATE_FILE_H

#include <string>

namespace Botan { class ECDSA_PrivateKey; }

namespace Security {

    class Key_file;

    class Certificate_file {
    public:
        Certificate_file(const std::string& certificate_filename);
        bool exists() const;
        operator bool() const;
        void create(const Key_file& key_file);
        const std::string& filename() const;
  
    private:
        std::string file_name;
    };

} // namespace Security


#endif  // CERTIFICATE_FILE_H