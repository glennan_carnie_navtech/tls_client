#include <algorithm>

#include "Server_credentials_manager.h"
#include "trace.h"


namespace Security {

    void Server_credentials_manager::set_certificate(const std::string& server_cert)
    {
        certificate = server_cert;
    }
    

    void Server_credentials_manager::set_key(const std::string& in_key, const std::string& in_passphrase)
    {
        key        = in_key;
        passphrase = in_passphrase;
    }


    bool Server_credentials_manager::load()
    {
        using std::move;

        Certificate_info certificate_info;
        
        try {
            if (passphrase.empty()) {
                certificate_info.key.reset(
                    Botan::PKCS8::load_key(key, rand_num_generator)
                );
            } 
            else {
                certificate_info.key.reset(
                    Botan::PKCS8::load_key(key, rand_num_generator, passphrase)
                );
            }
        }
        catch (const std::exception& e) {
            TRACE_MSG("Failed to load key");
            TRACE_MSG(e.what());

            return false;
        }
        
        try {
            Botan::DataSource_Stream input { certificate };
            
            while (!input.end_of_data()) {
                try {
                    certificate_info.certificates.push_back(Botan::X509_Certificate { input });
                }
                catch (const std::exception& e) {                    
                    if (input.end_of_data()) {
                        break;
                    }

                    TRACE_MSG("Failed to load certificate");
                    TRACE_MSG(e.what());

                    return false;
                }            
            }
        }
        catch (const std::exception& e) {
            TRACE_MSG("Failed to load certificate");
            TRACE_MSG(e.what());

            return false;
        }			

        if (certificate_info.certificates.size() == 0) {
            TRACE_MSG("Failed to load any certificate(s)");
            
            return false;
        }

        credentials.push_back(move(certificate_info));
        return true;
    }


    std::vector<Botan::X509_Certificate> Server_credentials_manager::cert_chain(
        const std::vector<std::string>& algos, 
        const std::string& type     [[maybe_unused]], 
        const std::string& hostname [[maybe_unused]])
    {
        using std::find;
        using std::begin;
        using std::end;

        for (const auto& credential : credentials) {
            if (find(begin(algos), end(algos), credential.key->algo_name()) == end(algos)) {
                continue;
            }

            if (hostname != "" && !credential.certificates[0].matches_dns_name(hostname)) {
                continue;
            }

            return credential.certificates;
        }

        return std::vector<Botan::X509_Certificate> { };
    }


    Botan::Private_Key* Server_credentials_manager::private_key_for(
        const Botan::X509_Certificate& certificate, 
        const std::string& type    [[maybe_unused]], 
        const std::string& context [[maybe_unused]])
    {
        for (auto const& credential : credentials) {
            if (certificate == credential.certificates[0]) {
                return credential.key.get();
            }
        }

        return nullptr;
    }


} // namespace Security