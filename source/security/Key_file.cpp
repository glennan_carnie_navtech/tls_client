
#include <fstream>

#include "botan/auto_rng.h"
#include "botan/ec_group.h"
#include "botan/pkcs8.h"

#include "Key_file.h"

namespace Security {

    Key_file::Key_file(const std::string& key_filename) : 
        file_name { key_filename }
    {
    }


    bool Key_file::exists() const
    {
        std::fstream key_file { };
        key_file.open(file_name);
        return !key_file.fail();
    }



    Key_file::operator bool() const
    {
        return exists();
    }


    void Key_file::create()
    {
        using std::fstream;
        using std::move;

        Botan::AutoSeeded_RNG rand_num_generator { };
        
        private_key = Key_pointer { new Botan::ECDSA_PrivateKey { rand_num_generator, Botan::EC_Group { "secp521r1" }  } };
        
        fstream key_file { };

        key_file.open(file_name, std::ios::out | std::ios::trunc);
        key_file << Botan::PKCS8::PEM_encode(*private_key);
        key_file.close();
    }


    const Botan::ECDSA_PrivateKey& Key_file::key() const
    {
        return *private_key;
    }


    const std::string& Key_file::filename() const
    {
        return file_name;
    }



} // namespace Security