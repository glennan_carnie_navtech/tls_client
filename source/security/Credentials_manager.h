#ifndef CREDENTIALS_MANAGER_H
#define CREDENTIALS_MANAGER_H

#include <string>
#include <vector>

#include "botan/auto_rng.h"
#include "botan/pkcs8.h"
#include "botan/credentials_manager.h"
#include "botan/x509self.h"
#include "botan/data_src.h"


namespace Security
{
	class Credentials_manager : public Botan::Credentials_Manager {
	public:
		
	protected:
		std::vector<Botan::Certificate_Store*> trusted_certificate_authorities(const std::string& type, const std::string& hostname) override;

	protected:
		Botan::AutoSeeded_RNG rand_num_generator { };

		using Cert_store_ptr       = std::unique_ptr<Botan::Certificate_Store>;
		using Cert_store_container = std::vector<Cert_store_ptr>;

		Cert_store_container cert_stores { };
	};

	
} // namespace Security

#endif // CREDENTIALS_MANAGER_H
