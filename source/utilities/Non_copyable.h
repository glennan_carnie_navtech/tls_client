#ifndef NON_COPYABLE_H
#define NON_COPYABLE_H

namespace Utility {

    // Classes realizing this interface can be moved
    // but not copied.
    //
    class Non_copyable {
    public:
        Non_copyable()                                  = default;
        ~Non_copyable()                                 = default;
        Non_copyable(Non_copyable&&)                    = default;
        Non_copyable& operator=(Non_copyable&&)         = default;
        
        Non_copyable(const Non_copyable&)               = delete;
        Non_copyable& operator=(const Non_copyable&)    = delete;
    };
}

#endif // NON_COPYABLE_H