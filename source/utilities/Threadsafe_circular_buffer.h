#ifndef THREADSAFE_CIRCULAR_BUFFER_H
#define THREADSAFE_CIRCULAR_BUFFER_H

#include <mutex>

#include "Circular_buffer.h"
#include "trace.h"


namespace Utility {

    // -------------------------------------------------------------------------------------------------------
    // Threadsafe circular buffer (FIFO)
    //
    template <typename T, std::size_t sz>
    class Threadsafe_circular_buffer : private Circular_buffer<T, sz> {
    public:
        template <typename U> void push(U&& in_val);
        template <template <typename U> class Container_Ty> void push(const Container_Ty<T>& in_vals);
        template <template <typename U> class Container_Ty> void push(Container_Ty<T>&& in_vals);
        template <typename Iterator_Ty> void push(Iterator_Ty first, Iterator_Ty last);
        template <typename Iterator_Ty> void push(Iterator_Ty first, std::size_t n);

        Threadsafe_circular_buffer& operator<<(const T& in_val);      
        Threadsafe_circular_buffer& operator<<(T&& in_val);
        template <template <typename> class Container_Ty> Threadsafe_circular_buffer& operator<<(const Container_Ty<T>& in_vals);
        template <template <typename> class Container_Ty> Threadsafe_circular_buffer& operator<<(Container_Ty<T>&& in_vals);

        std::optional<T> pop();
        std::vector<T>   pop_n(std::size_t n);
        template <typename Iterator_Ty> std::size_t pop_into(Iterator_Ty first, Iterator_Ty last);
        template <typename Iterator_Ty> std::size_t pop_n_into(Iterator_Ty first, std::size_t n);
        
        std::size_t size() const;
        std::size_t capacity() const;
        bool empty() const;

    private:
        using Buffer = Circular_buffer<T, sz>;

        mutable std::mutex mtx { };
    };


    template <typename T, std::size_t sz>
    template <typename U>
    void Threadsafe_circular_buffer<T, sz>::push(U&& in_val)
    {
        using std::lock_guard;
        using std::forward;

        lock_guard lock { mtx };
        Buffer::push(forward<U>(in_val));
    }


    template <typename T, std::size_t sz>
    template <template <typename> class Container_Ty>
    void Threadsafe_circular_buffer<T, sz>::push(const Container_Ty<T>& in_vals)
    {
        using std::lock_guard;

        lock_guard lock { mtx };
        Buffer::push(in_vals);
    }


    template <typename T, std::size_t sz>
    template <template <typename> class Container_Ty>
    void Threadsafe_circular_buffer<T, sz>::push(Container_Ty<T>&& in_vals)
    {
        using std::lock_guard;
        using std::move;

        lock_guard lock { mtx };
        Buffer::push(move(in_vals));
    }


    template <typename T, std::size_t sz>
    template <typename Iterator_Ty> 
    void Threadsafe_circular_buffer<T, sz>::push(Iterator_Ty first, Iterator_Ty last)
    {
        using std::lock_guard;
        using std::move;

        lock_guard lock { mtx };
        Buffer::push(first, last);
    }
    

    template <typename T, std::size_t sz>
    template <typename Iterator_Ty> 
    void Threadsafe_circular_buffer<T, sz>::push(Iterator_Ty first, std::size_t n)
    {
        using std::lock_guard;

        lock_guard lock { mtx };
        Buffer::push(first, n);
    }


    template <typename T, std::size_t sz>
    Threadsafe_circular_buffer<T, sz>& Threadsafe_circular_buffer<T, sz>::operator<<(const T& in_val)
    {
        using std::lock_guard;

        lock_guard lock { mtx };
        Buffer::operator<<(in_val);
        return *this;
    }


    template <typename T, std::size_t sz>   
    Threadsafe_circular_buffer<T, sz>& Threadsafe_circular_buffer<T, sz>::operator<<(T&& in_val)
    {
        using std::lock_guard;
        using std::move;

        lock_guard lock { mtx };
        Buffer::operator<<(move(in_val));
        return *this;
    }


    template <typename T, std::size_t sz>
    template <template <typename> class Container_Ty> 
    Threadsafe_circular_buffer<T, sz>& Threadsafe_circular_buffer<T, sz>::operator<<(const Container_Ty<T>& in_vals)
    {
        using std::lock_guard;

        lock_guard lock { mtx };
        Buffer::operator<<(in_vals);
        return *this;
    }


    template <typename T, std::size_t sz>
    template <template <typename> class Container_Ty> 
    Threadsafe_circular_buffer<T, sz>& Threadsafe_circular_buffer<T, sz>::operator<<(Container_Ty<T>&& in_vals)
    {
        using std::lock_guard;
        using std::move;

        lock_guard lock { mtx };
        Buffer::operator<<(move(in_vals));
        return *this;
    }


    template <typename T, std::size_t sz>
    std::optional<T> Threadsafe_circular_buffer<T, sz>::pop()
    {
        using std::lock_guard;

        lock_guard lock { mtx };
        Buffer::pop();
        return *this;
    }


    template <typename T, std::size_t sz>
    std::vector<T> Threadsafe_circular_buffer<T, sz>::pop_n(std::size_t n)
    {
        using std::lock_guard;

        lock_guard lock { mtx };
        return Buffer::pop_n(n);
    }
    

    template <typename T, std::size_t sz>
    template <typename Iterator_Ty> 
    std::size_t Threadsafe_circular_buffer<T, sz>::pop_into(Iterator_Ty first, Iterator_Ty last)
    {
        using std::lock_guard;

        lock_guard lock { mtx };
        return Buffer::pop_into(first, last);
    }


    template <typename T, std::size_t sz>
    template <typename Iterator_Ty> 
    std::size_t Threadsafe_circular_buffer<T, sz>::pop_n_into(Iterator_Ty first, std::size_t n)
    {
        using std::lock_guard;

        lock_guard lock { mtx };
        return Buffer::pop_n_into(first, n);
    }


    template <typename T, std::size_t sz>
    std::size_t Threadsafe_circular_buffer<T, sz>::size() const
    {
        using std::lock_guard;

        lock_guard lock { mtx };
        return Buffer::size();
    }


    template <typename T, std::size_t sz>
    std::size_t Threadsafe_circular_buffer<T, sz>::capacity() const
    {
        using std::lock_guard;

        lock_guard lock { mtx };
        return Buffer::capacity();
    }


    template <typename T, std::size_t sz>
    bool Threadsafe_circular_buffer<T, sz>::empty() const
    {
        using std::lock_guard;

        lock_guard lock { mtx };
        return Buffer::empty();
    }

} // namespace Utility

#endif // THREADSAFE_CIRCULAR_BUFFER_H