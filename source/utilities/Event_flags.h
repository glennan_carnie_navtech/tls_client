#ifndef EVENT_FLAGS_H
#define EVENT_FLAGS_H

#include <cstddef>
#include <cstdint>
#include <stdexcept>
#include <mutex>
#include <condition_variable>
#include "Bit_ops.h"

namespace Utility {

    // ------------------------------------------------------------------------------------------------------- 
    // Event_flags API
    //
    // Event_flags represents a set of N manually-reset signals.
    // Clients may block on one or more flags.  The class allows 
    // either conjunctive (wait all flags to be set) waiting, or 
    // disjunctive waiting (wait for *any* flag to be set).
    //
    // ----------------------------------------------------------
    // XXX = any       - Disjunctive wait
    // XXX = all       - Conjunctive wait
    //
    // wait_XXX()      - Blocking call; will block forever on
    //                   bit pattern
    //
    // try_wait_XXX()  - Non-blocking; will return false if pattern
    //                   match fails
    //
    //
    // ----------------------------------------------------------
    // Signal one or more event flags
    // 
    // void set(Bitmask bits_to_set);
    //
    // ----------------------------------------------------------
    // Read current state of event group
    //
    // operator Bitmask() const;
    //
    // ----------------------------------------------------------
    // Reset any signalled event flags
    //
    // void clear(Bitmask bits_to_clear);
    // void clear_all();
    //
    //
    // Return the state of a single event flag.  These operators
    // allow the Event_flags object to be accessed like an array;
    // each bit is indexed (from zero).
    // This allows code like the following:
    //
    // Utility::Event_flags<8> flags { };
    // flags[0] = 1;         // set
    // flags[0] = 0;         // clear
    // flags[1] = flags[0];  // copy state
    //
    // inline int  operator[](unsigned int flag) const;
    // inline Flag operator[](unsigned int flag);
    //
    // -------------------------------------------------------------------------------------------------------

    // -------------------------------------------------
    // Event flag exceptions
    //
    class invalid_flag : public std::out_of_range {
    public:
        invalid_flag(const char* str) : std::out_of_range(str) {}
        invalid_flag() : std::out_of_range("Invalid flag index") {}
    };


    // -------------------------------------------------
    // As the number of flags need not be a multiple of
    // 8, this compile-time function rounds up to the nearest
    // word length.
    //
    constexpr inline std::size_t to_nearest_byte(unsigned int num_bits)
    {
        if (num_bits <= 8)  return 8;
        if (num_bits <= 16) return 16;
        if (num_bits <= 32) return 32;
        return 0;
    }


    // -------------------------------------------------
    // Traits class for determining the underlying
    // type used to store the event flags.  Note there
    // is a maximum of 32 flags per event group.
    //
    template <std::size_t num_bits>
    struct Event_flag_traits {  };

    template <>
    struct Event_flag_traits<8>  { using type = std::uint8_t; };

    template <>
    struct Event_flag_traits<16> { using type = std::uint16_t; };

    template <>
    struct Event_flag_traits<32> { using type = std::uint32_t; };


    // -------------------------------------------------
    // Event_flags represents a set of N manually-reset
    // signals.
    //
    template <std::size_t num_flags>
    class Event_flags {
    public:
        using Bitmask = typename Event_flag_traits<to_nearest_byte(num_flags)>::type;

        Event_flags() = default;

        void wait_any    (const Bitmask flags_to_check);
        bool try_wait_any(const Bitmask flags_to_check);

        void wait_all    (const Bitmask flags_to_check);
        bool try_wait_all(const Bitmask flags_to_check);

        void set  (const Bitmask bits_to_set);
        void clear(const Bitmask bits_to_clear);
        void clear_all();
        operator Bitmask() const;


        // ----------------------------------------
        // Proxy for setting/clearing individual
        // event flags.
        //
        class Flag {
        public:
            // Signal / clear the flag
            //
            inline Flag& operator=(int val);
            inline Flag& operator=(const Flag& rhs);

            // Return the flag's state
            //
            inline operator int() const;

        private:
            friend class Event_flags;
            Flag(Event_flags& owner, unsigned num) : parent(&owner), flag(num) { }

            Event_flags* parent;
            unsigned     flag;
        };

        // Return the state of a single event flag
        //
        inline int  operator[](unsigned int flag) const;
        inline Flag operator[](unsigned int flag);

        // Copy / move policy
        //
        Event_flags(const Event_flags&)    = delete;
        void operator=(const Event_flags&) = delete;
        Event_flags(Event_flags&&)         = delete;
        void operator=(Event_flags&&)      = delete;
        
    private:
        std::condition_variable flag_set { };
        std::mutex mtx { };
        Bitmask state  { };
    };


    template <std::size_t num_flags>
    void Event_flags<num_flags>::set(const Event_flags<num_flags>::Bitmask bits_to_set)
    {
        std::lock_guard lock { mtx };
    
        state |= bits_to_set;
        flag_set.notify_all();
    }


    template <std::size_t num_flags>
    void Event_flags<num_flags>::clear(const Event_flags<num_flags>::Bitmask bits_to_clear)
    {
        std::lock_guard lock { mtx };
        
        state &= ~(bits_to_clear);
    }


    template <std::size_t num_flags>
    void Event_flags<num_flags>::clear_all()
    {
        clear(bit_range(0, num_flags));
    }


    template <std::size_t num_flags>
    int Event_flags<num_flags>::operator[](unsigned int flag) const
    {
        if ((flag < 0) || (flag >= num_flags)) throw invalid_flag { };
        return is_set(state, flag) ? 1 : 0;
    }


    template <std::size_t num_flags>
    typename Event_flags<num_flags>::Flag
    Event_flags<num_flags>::operator[](unsigned int flag)
    {
        if (flag >= num_flags) throw invalid_flag { };
        return Flag { *this, flag };
    }


    template <std::size_t num_flags>
    Event_flags<num_flags>::operator Event_flags<num_flags>::Bitmask() const
    {
        return state;
    }


    template <std::size_t num_flags>
    void Event_flags<num_flags>::wait_any(Event_flags<num_flags>::Bitmask flags_to_check)
    {
        Bitmask bit_mask = (flags_to_check & bit_range(0, num_flags));

        std::unique_lock lock { mtx };

        // AND the current bit pattern with the bits to check.
        // If any match this will yield a non-zero result.
        //
        while ((state & bit_mask) == 0) {
            flag_set.wait(lock);
        }
    }


    template <std::size_t num_flags>
    bool Event_flags<num_flags>::try_wait_any(const Event_flags<num_flags>::Bitmask flags_to_check)
    {
        Bitmask bit_mask = (flags_to_check & bit_range(0, num_flags));

        std::unique_lock lock { mtx };
        return ((state & bit_mask) != 0);
    }


    template <std::size_t num_flags>
    void Event_flags<num_flags>::wait_all(const Event_flags<num_flags>::Bitmask flags_to_check)
    {
        Bitmask bit_mask = (flags_to_check & bit_range(0, num_flags));

        std::unique_lock lock { mtx };

        // AND the current bit pattern with the bits to check.
        // If all the bits are set, this will yield the original
        // value of flags_to_check.
        //
        while ((state & bit_mask) != bit_mask) {
            flag_set.wait(lock);
        }
    }


    template <std::size_t num_flags>
    bool Event_flags<num_flags>::try_wait_all(const Event_flags<num_flags>::Bitmask flags_to_check)
    {
        Bitmask bit_mask = (flags_to_check & bit_range(0, num_flags));

        std::unique_lock lock { mtx };
        return ((state & bit_mask) == bit_mask);
    }


    template <std::size_t num_flags>
    typename Event_flags<num_flags>::Flag&
    Event_flags<num_flags>::Flag::operator=(int val)
    {
        if (val == 0) parent->clear(bit(flag));
        else          parent->set  (bit(flag));
        return *this;
    }


    template <std::size_t num_flags>
    Event_flags<num_flags>::Flag::operator int() const
    {
        return is_set(parent->state, flag) ? 1 : 0;
    }


    template <std::size_t num_flags>
    typename Event_flags<num_flags>::Flag&
    Event_flags<num_flags>::Flag::operator=(const Flag& rhs)
    {
        *this = static_cast<int>(rhs);
        return *this;
    }



} // namespace Utility


#endif // EVENT_FLAGS_H