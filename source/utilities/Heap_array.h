#ifndef HEAP_ARRAY_H
#define HEAP_ARRAY_H

#include <array>
#include <cstddef>

namespace Utility {

    // Heap_array is a fixed-sized array allocated on the heap.
    // It supports the basic features of std::array, but does
    // not support intialization lists.
    // 
    template <typename T, std::size_t sz>
    class Heap_array {
    public:
        using Iterator = typename std::array<T, sz>::iterator;

        Iterator begin()
        {
            return arr->begin();
        }

        Iterator end()
        {
            return arr->end();
        }

        std::size_t size() const
        {
            return sz;
        }

        T& operator[](int index)
        {
            return (*arr)[index];
        }

        const T& operator[](int index) const
        {
            return (*arr)[index];
        }

        T* data()
        {
            return arr->data();
        }

    private:
        std::unique_ptr<std::array<T, sz>> arr { new std::array<T, sz> { } };
    };


} // namespace Utility

#endif