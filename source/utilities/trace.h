// -------------------------------------------------------------------------------------
// NAVTECH ASSESSMENT 
//
// Filename:    Trace.h
//
// Overview:    This header contains debug/trace utility macros to aid with
//              debugging.
//              By default, these macros are disabled.  They can be enabled
//              by setting the global macro TRACE_ENABLED.
//
// Author:      Glennan Carnie
// Date:        Mar 2021
// Notes:       All code in this assessment is written in "Stroustrup" style, as 
//              recommended by the C++ Coding Guidelines -
//              (https://isocpp.github.io/CppCoreGuidelines/CppCoreGuidelines#S-naming)
//
// -------------------------------------------------------------------------------------

#ifndef TRACE_H_
#define TRACE_H_

    #ifdef TRACE_ENABLED
    #include <iostream>

    #define TRACE_MSG(msg)         std::cerr << "DEBUG: " << __func__ << " - " << msg << std::endl
    #define TRACE_VALUE(variable)  std::cerr << "DEBUG: " << __func__ << " - " << #variable << " : " << variable << std::endl

    #else
    #define TRACE_MSG(msg)
    #define TRACE_VALUE(variable)

    #endif

#endif