#ifndef MEMORY_TYPES_H
#define MEMORY_TYPES_H

#include <cstdint>

namespace Utility {

    namespace Memory_literals {

        class Kilobyte {
        public:
            constexpr Kilobyte() = default;
            constexpr Kilobyte(std::uint32_t sz) : value { sz }
            {
            }

            constexpr operator std::size_t() const
            {
                return static_cast<std::size_t>(value);
            }

        private:
            std::uint32_t value { };
        };


        class Megabyte {
        public:
            constexpr Megabyte() = default;
            constexpr Megabyte(std::uint32_t sz) : value { sz }
            {
            }

            constexpr operator std::size_t() const
            {
                return static_cast<std::size_t>(value);
            }

            constexpr operator Kilobyte() const
            {
                return Kilobyte { value * 1024 };
            }

        private:
            std::uint32_t value { };
        };


        constexpr Kilobyte operator""_kB(unsigned long long val)
        {
            return Megabyte { static_cast<std::uint32_t>(val) };
        }


        constexpr Megabyte operator""_MB(unsigned long long val)
        {
            return Megabyte { static_cast<std::uint32_t>(val) };
        }
    }
}


#endif // MEMORY_TYPES_H