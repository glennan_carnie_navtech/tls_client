
#include <algorithm>
#include <iterator>

#include "botan/tls_session_manager.h"
#include "botan/credentials_manager.h"
#include "botan/tls_policy.h"
#include "botan/tls_server.h"
#include "botan/tls_client.h"

#include "TLS_engine.h"
#include "Session.h"
#include "trace.h"


namespace Networking {

    // ------------------------------------------------------------------------------------------
    // TLS_engine
    //
    TLS_engine::TLS_engine(
        Botan::TLS::Session_Manager& tls_sess_mgr,
        Botan::Credentials_Manager&  tls_cred_mgr,
        Botan::TLS::Strict_Policy&   tls_policy,
        Session& owner
    ) :
        callbacks       { *this },
        session_mgr     { &tls_sess_mgr },
        credentials_mgr { &tls_cred_mgr },
        policy          { &tls_policy },
        session         { &owner }
    {
        encrypt_in.on_receive(
            [this](Message& msg)
            {
                try {
                    channel->send(msg.data(), msg.size());
                }
                catch (const std::exception& e) {
                    TRACE_MSG(e.what());
                    encryption_error();
                } 
            }
        );

        decrypt_in.on_receive(
            [this](Message& msg)
            {
                try {
                    channel->received_data(msg.data(), msg.size());
                }
                catch (const std::exception& e) {
                    TRACE_MSG(e.what());
                    encryption_error();
                }
            }
        );
    }


    void TLS_engine::open()
    {
        using std::make_unique;

        channel = make_unique<Botan::TLS::Client>(
            callbacks,
            *session_mgr,
            *credentials_mgr,
            *policy,
            Botan::system_rng()
        );

        // NOTE:  
        // The TLS component is only fully open
        // once the TLS handshake has completed.
    }
    
    
    void TLS_engine::close()
    {
        session->inactive(Session::Component::tls_engine);
    }


    void TLS_engine::encryption_error()
    {
        TRACE_MSG("Encryption error");
        session->report_internal_failure(Session::Component::tls_engine);
    }
    
    
    void TLS_engine::decryption_error()
    {
        TRACE_MSG("Decryption error");
        session->report_internal_failure(Session::Component::tls_engine);
    }


    // ------------------------------------------------------------------------------------------
    // TLS callbacks
    //
    TLS_engine::Callbacks::Callbacks(TLS_engine& owner) :
        tls_engine { &owner }
    {
    }


    void TLS_engine::Callbacks::tls_record_received(uint64_t rec_no [[maybe_unused]], const uint8_t buf[], size_t buf_len)
    {
        using std::copy_n;
        using std::move;
        using std::back_inserter;

        Message msg { };
        msg.reserve(buf_len);
        copy_n(buf, buf_len, back_inserter(msg));
        tls_engine->decrypt_out.post(move(msg));
    }


    void TLS_engine::Callbacks::tls_emit_data(const uint8_t buf[], size_t buf_len)
    {
        using std::copy_n;
        using std::move;
        using std::back_inserter;
        
        Message msg { };
        msg.reserve(buf_len);
        copy_n(buf, buf_len, back_inserter(msg));
        tls_engine->encrypt_out.post(move(msg));
    }


    bool TLS_engine::Callbacks::tls_session_established(const Botan::TLS::Session& session [[maybe_unused]])
    {
        return true;
    }


    void TLS_engine::Callbacks::tls_alert(Botan::TLS::Alert alert [[maybe_unused]])
    {
        TRACE_MSG("TLS alert signalled");
        TRACE_VALUE(alert.type_string());
        
        if (alert.is_fatal()) {
            assert(false);
        }
    }


    void TLS_engine::Callbacks::tls_session_activated()
    {
        // TLS is only active once the TLS handshake
        // is fully complete.
        //
        tls_engine->session->active(Session::Component::tls_engine);
    }


    void TLS_engine::Callbacks::tls_verify_cert_chain(
        const std::vector<Botan::X509_Certificate>& cert_chain,
        const std::vector<std::shared_ptr<const Botan::OCSP::Response>>& ocsp_responses [[maybe_unused]],
        const std::vector<Botan::Certificate_Store*>& trusted_roots                     [[maybe_unused]],
        Botan::Usage_Type usage                                                         [[maybe_unused]],
        const std::string& hostname                                                     [[maybe_unused]],
        const Botan::TLS::Policy& policy                                                [[maybe_unused]]
    )
    {
        if (cert_chain.empty()) {
            throw Botan::Invalid_Argument("Certificate chain was empty");
        }

        Botan::Path_Validation_Restrictions restrictions(
            policy.require_cert_revocation_info(),
            policy.minimum_signature_strength()
        );

        Botan::Path_Validation_Result result = Botan::x509_path_validate(
            cert_chain,
            restrictions,
            trusted_roots
            // hostname,
            // usage,
            // std::chrono::system_clock::now(),
            // ocsp_timeout,
            // ocsp_responses
        );

        TRACE_MSG("Certificate validation status: ");
        TRACE_MSG(result.result_string());
        
        if (result.successful_validation()) {
            auto status = result.all_statuses();

            if (status.size() > 0 && status[0].count(Botan::Certificate_Status_Code::OCSP_RESPONSE_GOOD)) {
                TRACE_MSG("Valid OCSP response for this server");
            }
        }
    }

} // namespace Networking