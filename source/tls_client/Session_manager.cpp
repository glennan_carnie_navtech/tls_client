#include "Session_manager.h"
#include "Security_config.h"
#include "Certificate_file.h"
#include "Key_file.h"

using namespace asio::ip;
using namespace std;

namespace Networking {

    Session_manager::Session_manager(Security::Config& cfg) :
        config { &cfg }
    {
    }


    void Session_manager::create_session(asio::io_context& context, asio::ip::tcp::socket socket)
    {
        lock_guard lock { mtx };

        // Lazy instantiation of TLS management
        // components
        //
        if (tls_session_mgr == nullptr) {
            initialise_tls();
        }

        auto id = next_id();

        sessions.emplace(
            id, 
            Session::create(
                context,
                move(socket),
                *tls_session_mgr,
                *credentials_mgr,
                *tls_policy,
                *this, 
                id
            )
        );
    }


    Session::Owning_ptr Session_manager::get_session_ptr(Session::ID id)
    {
        lock_guard lock { mtx };

        if (auto it = sessions.find(id); it != end(sessions)) {
            return it->second;
        }
        else {
            return nullptr;
        }
    }



    bool Session_manager::remove(Session::ID id)
    {
        lock_guard lock { mtx };

        if (auto it = sessions.find(id); it != end(sessions)) {
            sessions.erase(it);
            return true;
        }
        else {
            return false;
        }
    }


    Session::ID Session_manager::next_id()
    {
        static Session::ID id { 1 };
        return id++;
    }



    void Session_manager::initialise_tls()
    {
        using std::make_unique;
        using Security::Key_file;
        using Security::Certificate_file;
        using Security::Server_credentials_manager;
        using Security::TLS_policy;


        Key_file         key_file  { config->key };
        Certificate_file cert_file { config->certificate };

        if (!key_file)  key_file.create();
        if (!cert_file) cert_file.create(key_file);

        credentials_mgr = make_unique<Server_credentials_manager>();
        tls_session_mgr = make_unique<Botan::TLS::Session_Manager_In_Memory>(Botan::system_rng());
        tls_policy      = make_unique<TLS_policy>();
        assert(credentials_mgr);
        assert(tls_session_mgr);
        assert(tls_policy);

        credentials_mgr->set_certificate(config->certificate);
        credentials_mgr->set_key(config->key, config->passphrase);

        if (!credentials_mgr->load()) {
            TRACE_MSG("TLS credentials failed to load");
        }
    }
}