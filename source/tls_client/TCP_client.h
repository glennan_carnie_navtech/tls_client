#ifndef TCP_CLIENT_H
#define TCP_CLIENT_H

#include <string>
#include <thread>
#include "asio.hpp"


namespace Networking {

    class Session_manager;

    class TCP_client {
    public:
        TCP_client(Session_manager& ses_mgr);
        ~TCP_client();
    
        void connect_to(const std::string& address, const std::string& port);

    private:
        void start();
        void run();

        asio::io_context io_context;
        Session_manager* session_mgr;

        std::thread t;
        bool started { };
    };

} // namespace Networking

#endif // TCP_CLIENT_H