#include <algorithm>
#include <iostream>
#include <cassert>
#include <charconv>
#include <cstdlib>

#include "Message_generator.h"
#include "trace.h"

using namespace std;

namespace Networking {

    array<const char*, 13> Message_generator::data {
        "Hello there!",
        "To be, or not to be, that is the question.",
        "In C++ no-one can hear you scream.",
        "This is a dull message.",
        "Welcome to my world.",
        "A message arrives PRECISELY when it means to.",
        "A more exciting message (probably).",
        "Filling time and space.",
        "This message is just for shizzles.",
        "Good grief, Charlie Brown!",
        "BOOM, BOOM, BOOM.  BOOM. BOOM.",
        "Always be yourself.  Unless you can be Batman.",
        "If you can see this, you're too close."
    };


    string Message_generator::random_string() const
    {
        return random_string(0);
    }


    string Message_generator::random_string(std::size_t min_sz_bytes) const
    {
        string msg_data { };

        do {
            msg_data += string { data[(std::rand() % 13)] };
            msg_data += "-";
        } while (msg_data.size() < min_sz_bytes);
        
        msg_data += "*||*";

        return msg_data;
    }


    Colossus::TCP_message::Type Message_generator::random_type() const
    {
        // Not *actually* random...
        //
        return Colossus::TCP_message::Type::config;

        // random_device rand_num { };
        // return static_cast<Message_type>(rand_num() % 3);
    }


    std::string Message_generator::fixed_string(std::size_t sz) const
    {
        string msg_data { };

        for (; sz > 0; --sz) {
            msg_data += '*';
        }

        return msg_data;
    }


} // namespace Networking