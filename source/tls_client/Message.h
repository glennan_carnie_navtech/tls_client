#ifndef MESSAGE_H
#define MESSAGE_H

#include <vector>
#include <cstdint>

#include "Non_copyable.h"
#include "trace.h"


namespace Networking {

    template <typename T>
    class Dynamic_buffer : public std::vector<T> {
    public:
        using std::vector<T>::vector;

        // ~Dynamic_buffer()
        // {
        //     if (!std::vector<T>::empty()) {
        //         TRACE_MSG("Deleting data");
        //     }
        // }

        Dynamic_buffer(Dynamic_buffer&&)                    = default;
        Dynamic_buffer& operator=(Dynamic_buffer&&)         = default;
        
        Dynamic_buffer(const Dynamic_buffer&)               = delete;
        Dynamic_buffer& operator=(const Dynamic_buffer&)    = delete;

    };


    // A Message is a raw, uninterpreted, container
    // of data. Messages are sent across the network
    // to be interpreted by clients and servers
    //
    using Message = Dynamic_buffer<std::uint8_t>;
   
} // namespace Networking

#endif // MESSASGE_H