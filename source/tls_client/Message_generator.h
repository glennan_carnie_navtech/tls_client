#ifndef MESSAGE_GENERATOR_H
#define MESSAGE_GENERATOR_H

#include <string>
#include <cstdint>
#include <array>
#include <vector>

#include "Colossus_message.h"
#include "Message.h"

// --------------------------------------------------------------------------------------------------
//
namespace Networking {

    class Message_generator {
    public:
        std::string random_string() const;
        std::string random_string(std::size_t min_sz_bytes) const;
        std::string fixed_string(std::size_t sz) const;
        Colossus::TCP_message::Type random_type() const;

    private:
        static std::array<const char*, 13> data;
    };

} // namespace Networking

#endif // MESSAGE_GENERATOR_H