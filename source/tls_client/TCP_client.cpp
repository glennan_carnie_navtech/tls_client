#include <cassert>
#include <iostream>
#include "TCP_client.h"
#include "Session_manager.h"
#include "trace.h"

using namespace asio;
using namespace std;

namespace Networking {

    TCP_client::TCP_client(Session_manager& ses_mgr) :
        session_mgr { &ses_mgr }
    {
    }


    TCP_client::~TCP_client()
    {
        io_context.stop();
    }


    void TCP_client::start()
    {
        t = std::thread { bind(&TCP_client::run, this) };
        t.detach();
    }


    void TCP_client::run()
    {
        using Work_guard = asio::executor_work_guard<asio::io_context::executor_type>;

        Work_guard work_guard { io_context.get_executor() };
        io_context.run();
    }

    
    void TCP_client::connect_to(const std::string& address, const std::string& port)
    {
        assert(session_mgr != nullptr);

        if (!started) start();

        TRACE_MSG("Connecting");

        unsigned fail_count { 0 };
        bool connected { false };

        do {
            try {
                ip::tcp::socket         socket   { io_context };
                asio::ip::tcp::resolver resolver { io_context };

                asio::ip::address addr { };

                auto endpoints = resolver.resolve(address, port);
                asio::connect(socket, endpoints);

                TRACE_MSG(socket.remote_endpoint().address().to_string());

                session_mgr->create_session(io_context, move(socket));

                connected = true;

                TRACE_MSG("Connected");
            }
            catch (std::exception& ex) {
                cout << "Connection exception: " << ex.what() << endl;
                ++fail_count;
                TRACE_VALUE(fail_count);

                if (fail_count == 3) throw;
            }
        } while (!connected);
    }

} // namespace Networking