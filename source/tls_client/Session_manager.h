#ifndef SESSION_MANAGER_H
#define SESSION_MANAGER_H

#include <unordered_map>
#include <mutex>
#include <memory>

#include "botan/system_rng.h"
#include "botan/tls_session_manager.h"

#include "asio.hpp"

#include "Session.h"


#include "Server_credentials_manager.h"
#include "Policy.h"


namespace Security { class Config; }


namespace Networking {

    class Session_manager {
    public:
        Session_manager(Security::Config& cfg);

        void create_session(asio::io_context& context, asio::ip::tcp::socket socket);
        Session::Owning_ptr get_session_ptr(Session::ID id);
        bool remove(Session::ID id);

    private:
        friend class Session;

        void initialise_tls();
        Session::ID next_id();

        Security::Config* config;

        std::mutex mtx { };
        std::unordered_map<Session::ID, Session::Owning_ptr> sessions { };
        
        // Security (TLS) components
        // Instantiated on creation of first session
        //
        std::unique_ptr<Security::Server_credentials_manager>  credentials_mgr { nullptr };
        std::unique_ptr<Botan::TLS::Session_Manager>           tls_session_mgr { nullptr };
        std::unique_ptr<Botan::TLS::Strict_Policy>             tls_policy      { nullptr };
    };

} // namespace Networking

#endif // SESSION_MANAGER_H