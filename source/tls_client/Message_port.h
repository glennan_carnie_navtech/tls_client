#ifndef MESSAGE_PORT_H
#define MESSAGE_PORT_H

#include "Port.h"
#include "Message.h"

namespace Networking {

    using Message_port = Utility::Port<Message>;

}

#endif // MESSAGE_PORT_H