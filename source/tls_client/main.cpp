#if 0

#include <vector>
#include <iostream>

#include "Circular_buffer.h"

using namespace std;
using namespace Utility;




int main()
{
    Circular_buffer<int, 8> buffer { };
    vector v { 1, 2, 3, 4, 5, 6, 7, 8, 9 };
    buffer.push(v);

    cout << buffer.size() << endl;
    cout << buffer.capacity() << endl;
    cout << buffer.pop().value() << endl;
}
















#else
// --------------------------------------------------------------------------------------------------
//
#include <iostream>
#include <chrono>
#include <thread>
#include <random>
#include <array>

#include "TCP_client.h"
#include "Session_manager.h"
#include "Message_generator.h"
#include "Config.h"
#include "trace.h"

using namespace std;
using namespace std::chrono_literals;


int main(int argc [[maybe_unused]], char* argv[] [[maybe_unused]])
{
    using std::cout;
    using std::endl;

    cout << "Running client" << endl;

    // Construct the system
    //
    Networking::Config            config { };
    Networking::Session_manager   session_mgr { config.client_security };
    Networking::TCP_client        client      { session_mgr };
    

    client.connect_to(
        config.client_config.server_addr, 
        config.client_config.port
    );

    auto session = session_mgr.get_session_ptr(1);
    assert(session);

    cout << "Wait for it..." << endl;
    // this_thread::sleep_for(30s);
    cout << "Opening session..." << endl;
    session->open();
    cout << "Session open!" << endl;

#ifdef SENDER
    Networking::Message_generator message_generator { };

    thread sender_thread {
        [&session_mgr, &session, &message_generator]()
        {
            for (int i { 0 }; i < 100'000; ++i) {
                
                // cerr << "Message: " << i << "\r";

                session = session_mgr.get_session_ptr(1);
                if (!session || !session->is_open()) break;
                
                session->send_message(
                    message_generator.random_type(), 
                    message_generator.fixed_string(1003)
                );
                    
                this_thread::sleep_for(1ms);
            }
            cerr << "\n\rSend finished" << endl;
            // session->close();
            while (true);
        }

    };
    sender_thread.join();

#else
    // thread closer_thread {
    //     [&session]()
    //     {
    //         this_thread::sleep_for(10s);
    //         session->close();
    //     }
    // };


    thread receiver_thread {
        [&session]()
        {
            size_t num_messages { 0 };

            while (session->is_open()) {
                auto msg = session->get_message();
                ++num_messages;
                if (num_messages % 1 == 0) {
                    cerr << "Messages received: " << num_messages << "\r";
                }

                // auto colossus_msg = Networking::Colossus::TCP_message::overlay_onto(msg.data());
                
                // cerr << "Signature: ";
                // cerr << hex;
                // for_each(
                //     colossus_msg->signature_begin(), 
                //     colossus_msg->signature_end(), 
                //     [](const auto& chr) { cerr << static_cast<unsigned>(chr) << " "; }
                // );
                // cerr << dec;
                // cerr << endl;

                // cerr << "Type:      " << static_cast<unsigned int>(colossus_msg->type()) << endl;
                // cerr << "Payload:   " << colossus_msg->payload_size() << endl;
            }
        }
    };

   // closer_thread.join();
    receiver_thread.join();
#endif // SENDER

    cout << "Finished" << endl;
}

#endif