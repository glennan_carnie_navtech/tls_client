#ifndef COLOSSUS_MESSAGE_H
#define COLOSSUS_MESSAGE_H

#include <cstdint>
#include <array>

namespace Networking {

    namespace Colossus {

        // Structure overlay for interpreting messages
        // This struct is not meant to be instantiated.
        //
        class TCP_message {
        public:
            enum class Type : std::uint8_t {
                config                  = 10,
                confgi_request          = 20,
                start_fft               = 21,
                stop_fft                = 22,
                start_health            = 23,
                stop_health             = 24,
                reset_rf_health         = 25,
                fft_data                = 30,
                high_prec_fft_data      = 31,
                health                  = 40,
                contour_update          = 50,
                system_restart          = 76,
                logging_levels          = 90,
                logging_levels_req      = 100,
                start_nav_data          = 120,
                stop_nav_data           = 121,
                set_nav_threshold       = 122,
                navigation_data         = 123,
                set_nav_gain_and_offset = 124,
                calibrate_accelerometer = 125,
                start_accelerometer     = 126,
                stop_accelerometer      = 127,
                accelerometer_data      = 128
            };

            // Helper functions for extracting data
            //
            using Iterator       = std::uint8_t*;
            using Const_iterator = const std::uint8_t*;

            template <typename From_Ty>
            static TCP_message* overlay_onto(From_Ty* from) { return reinterpret_cast<TCP_message*>(from); }

            bool          is_valid() const;
            std::size_t   size() const;
            std::uint32_t payload_size() const;
            void          payload_size(std::uint32_t sz);

            static constexpr std::size_t header_size() { return sizeof(Header); }

            Type type() const;
            void type(Type t);

            Iterator       begin();
            Const_iterator begin() const;
            Iterator       end();
            Const_iterator end() const;

            Iterator       signature_begin();
            Const_iterator signature_begin() const;
            Iterator       signature_end();
            Const_iterator signature_end() const;

            Iterator       payload_begin();
            Const_iterator payload_begin() const;
            Iterator       payload_end();
            Const_iterator payload_end() const;
        
        private:
            bool is_signature_valid() const;
            
            // Message layout
            // (NOTE - payload is variable-sized)
            //
            #pragma pack(1)   
            struct Header {
                std::uint8_t  signature[16];
                Type          id;
                std::uint32_t payload_size;
            };
            #pragma pack()

            Header header;
            std::uint8_t payload[];
        };

        static_assert(sizeof(TCP_message) == 21); 


        // Configuration message - 
        // Consists of:
        // - Header
        // - Variable-sized protocol buffer payload
        //
        struct Configuration_message {
            struct Header {
                std::uint16_t azimuth_samples;
                std::uint16_t bin_size;
                std::uint16_t range_in_bins;
                std::uint16_t encoder_size;
                std::uint16_t rotation_speed;
                std::uint16_t packet_rate;
                std::uint32_t range_gain;
                std::uint32_t range_offset;
            } __attribute__((packed));;

            Header header;
            std::uint8_t protocol_buffer[];

            // Helper functions for extracting data
            //
            using Iterator       = std::uint8_t*;
            using Const_iterator = const std::uint8_t*;

            Iterator protocol_buffer_begin()
            {
                return reinterpret_cast<Iterator>(reinterpret_cast<Iterator>(&header) + sizeof(header));
            }

            Const_iterator protocol_buffer_begin() const
            {
                return reinterpret_cast<Const_iterator>(reinterpret_cast<Const_iterator>(&header) + sizeof(header));
            }


        }__attribute__((packed));

        static_assert(sizeof(Configuration_message) == 20);

    } // namespace Colossus

} // namespace Networking

#endif // COLOSSUS_MESSAGE_H