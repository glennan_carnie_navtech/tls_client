#ifndef SERVER_CONFIG_H
#define SERVER_CONFIG_H

#include <string>
#include <cstdint>

namespace Networking {

    struct Server_config {
        std::string   certificate  { "/systemconfig/device.cert" };
        std::string   key          { "/systemconfig/device.key" };
        std::string   passphrase   { "" };
        std::uint16_t listen_port  { 56317 };

        void store() { }
        void load()  { }
    };

} // namespace Networking

#endif // SERVER_CONFIG_H