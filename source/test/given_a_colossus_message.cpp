#include <array>
#include <cstdint>

#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include "Circular_buffer.h"

using namespace std;

class GivenAColossusMessage : public ::testing::Test {
protected:
    GivenAColossusMessage() = default;

    std::array<std::uint8_t, 32> message {
        0x00, 0x01, 0x03, 0x03, 0x09, 0x09, 0x0F, 0x0F, 
        0x1F, 0x1F, 0x6F, 0x6F, 0x8F, 0x8F, 0xFE, 0xFE,
        10,
        00, 00, 00, 11,
        '+','+','M','e','s','s','a','g','e','+','+'
    };
};


TEST_F(GivenAColossusMessage, AValidSizeIsReturned)
{
    auto msg = Networking::Colossus::TCP_message::overlay_onto(begin(message));

    ASSERT_EQ(msg->size(), 32);
}


TEST_F(GivenAColossusMessage, AValidTypeIsReturned)
{
    auto msg = Networking::Colossus::TCP_message::overlay_onto(begin(message));

    ASSERT_EQ(msg->type(), 10);
}


TEST_F(GivenAColossusMessage, BeginAndEndIteratorsAreValid)
{
    auto msg = Networking::Colossus::TCP_message::overlay_onto(begin(message));

    ASSERT_EQ(msg->begin(), begin(message));
    ASSERT_EQ(msg->end(), end(message)));
}


TEST_F(GivenAColossusMessage, AValidPayloadSizeIsReturned)
{
    auto msg = Networking::Colossus::TCP_message::overlay_onto(begin(message));

    ASSERT_EQ(msg->payload_size(), 11);
}


TEST_F(GivenAColossusMessage, ValidPayloadIteratorsAreReturned)
{
    auto msg = Networking::Colossus::TCP_message::overlay_onto(begin(message));

    ASSERT_EQ(msg->payload_begin(), begin(message) + 21);
    ASSERT_EQ(msg->payload_end(), end(message));
}


TEST_F(GivenAColossusMessage, ValidSignatureIteratorsAreReturned)
{
    auto msg = Networking::Colossus::TCP_message::overlay_onto(begin(message));

    ASSERT_EQ(msg->signature_begin(), begin(message));
    ASSERT_EQ(msg->signature_end(), begin(message) + 21);
}


TEST_F(GivenAColossusMessage, MessageValidationIsCorrect)
{
    auto msg = Networking::Colossus::TCP_message::overlay_onto(begin(message));

    ASSERT_TRUE(msg->is_valid());
}
