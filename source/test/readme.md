Feabhas test framework
======================

This is a very simple, header-only testing framework inspired by Doctest / Catch2.  Think of it as 'Doctest ultra-lite' :-)

Requirements
------------
feabhas_test requires at least C++14.
At present it has only been tested with
gcc 8.1
arm-none-eabi-gcc 8.1

Installation
------------

As the framework is header-only you only have to make the header file visible to the compiler.  You have two options:
* place it in your /src folder
* Add the path to the header file to your include path.

Running tests
-------------
In normal usage, tests will be automatically registered and run when your program executes.  To do this feabhas_test can provide its own main() function.
To enable this, you must define the macro `TEST_FRAMEWORK_GENERATE_MAIN` in one - and only one! - source file.  We recommend you use a separate file, like `test_main.cpp`.  For example:

```
// test_main.cpp
//
#define TEST_FRAMEWORK_GENERATE_MAIN
#include "feabhas_test.h"
```

That's it!

If you want to write the main() function yourself (for example, so as not to confuse readers who may be wondering how/why all this code is automagically executing!) you can write the code yourself.  Again, it's pretty trivial:

```
// test_main.cpp
//
#include "feabhas_test.h"

INIT_TEST_FRAMEWORK(feabhas_test_framework);

int main()
{
    RUN_ALL_TESTS(feabhas_test_framework);
}
```
The label `feabhas_test_framework` can be any valid variable name you like; you just have to ensure the label is the same in both macros!

By default, TEST_CASEs are disabled - that is, they exist as simple functions, and the testing framework does nothing.

In order to enable testing you must set the global macro `TESTING_ENABLED`.  We recommend doing this as a compile option in your build script.

Getting started
---------------
Creating tests is very simple.  Tests are created as 'pseudo-functions', called TEST_CASEs.  They can be placed in any source (.cpp) file.

A TEST_CASE must contain a description string.  This should, ideally, describe the test being performed; or the functionality being verified.

You must include the "feabhas_test.h" header.  It is also recommend, for clarity, that you make the Feabhas::Test namespace visible.


```
#include "feabhas_test.h"

using namespace Feabhas::Test;

TEST_CASE("Adding to a full buffer will fail")
{
    // Test code here...
}
```

TEST_CASEs are self-registering with the testing framework.  The act of writing the TEST_CASE causes it to be added to the set of tests that are run; there is no need to do anything but write the tests.

Testing asserts
---------------
Feabhas_test uses a basic matcher framework to allow you to verify the results of code.  The aim is to make the test assertions (relatively) intuitive and human-readable.

Test assertions use a `verify_that` function to perform their checks.  The function takes two parameters
   * the value to be assessed
   * a 'matcher' that performs an appropriate comparison.

The most likely comparison is for equality.  We use the `equal_to` matcher to check for equality, as follows
```
verify_that(x, equal_to(y));
```
### Synonyms

There are several matchers for equality.  They can be used interchangeably.  Your choice of matcher depends entirely on
how you wish to express your assertion.
```
verify_that(x, equal_to(y));
verify_that(x, equals(y));
```

The is() matcher, when used with a value is the same as 'equal_to'; when
used with a matcher, simply applies the supplied matcher; so the following
are equivalent:

```
verify_that(x, equal_to(y));
verify_that(x, is(y));
verify_that(x, is(equal_to(y)));
```

### Floating-point comparisons

Floating point numbers are compared for 'approximately equal'.
How 'approximately' can be adjusted by supplying a `within` value to the matcher:
```
verify_that(16.01, equals(16.0).within(0.1));
```

### Greater-than, less-than

Besides equality-checks, values may be compared for greater-than or less-than.
```
verify_that(18.6, is(greater_than(14.5)));
verify_that(100, is(less_than(101)));
```

### Range checks
A test value can be tested to see if it is within a particular range.  The test is inclusive; that is:
lower <= test_value <= upper
```
verify_that(101, is(in_range(99, 100)));  // false
verify_that(99,  is(in_range(99, 100)));  // true
verify_that(100, is(in_range(99, 100)));  // true
```

### String comparisons

String values can be compared in the same way as integers.
```
std::string str { "Hello" };
verify_that(str, is(equal_to("Hello")));         // true
verify_that(str, is(less_than("World")));        // true
verify_that(str, is(greater_than("Jumanji!")));  // false
```

Strings can also be verified by regular expressions, using the `regular_expression` matcher (or its shorter-form version `reg_ex`):
```
std::string str { "Hello" };
verify_that(str, is(regular_expression("[Hh]ello")));  // true
verify_that(str, is(equal_to(reg_ex("[Hh]ello"))));    // Same as above
```

### Object identity comparisons
You can check whether two identifiers refer to the same object with an identity comparison check.  This is useful when you are dealing indirectly with objects, for example via pointers or references.
```
class ADT { };

ADT& returns_lhs(ADT& lhs, ADT& rhs);

TEST_CASE("Check that object!")
{
    ADT adt1 { 1 };
    ADT adt2 { 2 };

    verify_that(adt1,  is(same_object_as(adt2)));  // false
    verify_that(&adt1, is(same_object_as(&adt2))); // compare pointers
    
    auto& adt3 = return_lhs(adt1, adt2);
    verify_that(adt3, is(same_object_as(adt1)));   // true
}
```

### "Anything"

The value 'anything' will match any value; so any matcher
given the parameter 'anything' will return true.
```
verify_that(some_variable, is(anything)); // Always true
```


### Function calls

Typically, you will be verifying values but, as part of a verification assertion you may want to call a function.  You can do this with the `invoking` call:

```
int func();

TEST_CASE("Call that function!")
{
    verify_that(invoking(func), returns(0));
}
```

The `returns` clause can be used with any other matchers; for example:
```
double func_a();
int    func_b();

TEST_CASE("Check those functions!")
{
    verify_that(invoking(func_a), returns(10.0).within(0.0001));
    verify_that(invoking(func_b), returns(greater_than(1)));
    verify_that(invoking(func_b), returns(anything));
}
```

A function can be invoked with arguments by supplying a second clause, `with_args`.  Note: the number of arguments must match the number of parameters for the function.

```
int func(int a, int b);

TEST_CASE("Call function with arguments")
{
    verify_that(invoking(func, with_args(21, 22)), returns(greater_than(0)));
}
```

If the function may throw an exception, this may be tested for, using the `throws` clause instead of the `returns` clause.
```
void func(int a, int b);

TEST_CASE("Function throws an exception")
{
    verify_that(invoking(func, with_args(21, 22)), throws(std::out_of_range));
}
```
Points to note:
* If the function does not throw an exception the assertion will fail (you told it to expect an exception!)
* If the exception thrown is derived from the expected exception the assertion will pass.  So, for example:
```
void func() { throw std::out_of_range { "Broken!" }; }

TEST_CASE("Will it fail?!")
{
    verify_that(invoking(func), throws(std::exception));  // <= Assertion will pass.
}
```

### Aborting and ignoring tests
It may make sense (for example in a test-driven development environment) to have tests that are not yet complete.  In such cases you mark the test as 'ignored'.  The `ignore_test` call can optionally be given a text string (to give diagnostic feedback).
```
TEST_CASE("Really complex test")
{
    ignore_test("Not finished yet");
}
```

An ignored test is not considered 'passed' OR 'failed'.  It is recorded as a separate status.

In some cases, the test case may reach a state where continuing would make no sense. In these situations you may choose to 'abort' the test.  When you abort a test you must provide a flag that indicates the test's result.  Valid values are:
* Test_result::pass
* Test_result::fail
* Test_result::ignore

You may also optionally provide a description string to provide diagnostic information.
```
TEST_CASE("Complex test")
{
    if (some_condition) {
        verify_that(invoking(do_stuff), returns(0));
    }
    else {
        abort_test(Test_result::fail, "Disaster strikes!");
    }
}
```

Syntax summary
--------------
```
verify_that( 
    <variable_name>, 
    <condition>( <expected_value> ) 
);

verify_that( 
    <variable_name>, 
    is ( <expected_value> )
);

verify_that( 
    <variable_name>, 
    is ( <condition>( <expected_value> ) ) 
);

verify_that( 
    <string>, 
    is ( regular_expression( <search_pattern> ) ) 
);

verify_that( 
    invoking( <function_name> ), 
    returns ( <expected_value> ) 
);

verify_that( 
    invoking( <function_name> ), 
    returns ( <condition>( <expected_value> ) ) 
);

verify_that( 
    invoking( <function_name>, with_args( <argument_list> ) ), 
    returns ( <expected_value> )
);

verify_that( 
    invoking( <function_name>, with_args( <argument_list> ) ), 
    returns ( <condition>( <expected_value> ) )
);

verify_that( 
    invoking( <function_name> ), 
    throws  ( <exception_type> ) 
);

verify_that( 
    invoking( <function_name>, with_args( <argument_list> ) ), 
    throws  ( <exception_type> )
);

ignore_test();
ignore_test( <diagnostic_text> );

abort_test( <Test_result_flag> );
abort_test( <Test_result_flag>, <diagnostic_text> );
```

NOTES:
------ 
At present there are a limited number of matchers available.  The framework currently lacks the ability to do string-matching, binary bit-checking and many others.  These will be added as soon as possible.


