#!/bin/sh
#
# Docker image
#
DOCKER_IMAGE=ubuntu_botan:1.0
NETWORK=client_side
IP_ADDR=172.19.1.10

docker run --rm -it -v $(pwd):/usr/asio -w /usr/asio --name $1  --cap-add=NET_ADMIN --network $NETWORK --ip $IP_ADDR $DOCKER_IMAGE